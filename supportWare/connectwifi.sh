#!/bin/bash
ssid="SEUP_NETWORK"
systemctl restart wpa_supplicant.service
systemctl restart connman.service
#echo "Disabling wifi"
/usr/lib/connman/test/test-connman disable wifi
#echo "Enabling wifi"
/usr/lib/connman/test/test-connman enable wifi
#echo "Scanning wifi"
/usr/lib/connman/test/test-connman scan wifi || (sleep 5 && /usr/lib/connman/test/test-connman scan wifi) || (sleep 5 && /usr/lib/connman/test/test-connman scan wifi && exit 1)
#echo "Services:"
#ID=$(/usr/lib/connman/test/test-connman services | grep $ssid | cut -d "{" -f2 | cut -d "}" -f1)
ID="wifi_74da388d1c4b_534555505f4e4554574f524b_managed_psk"
./activateAgent.sh &
(flag=$(/usr/lib/connman/test/test-connman connect $ID)) || (flag=$(/usr/lib/connman/test/test-connman connect $ID))
exit $flag
