#!/bin/bash

line="0 * * * * /var/lib/hemAppBin/supportWare/cleanHemLog.sh"

if ! crontab -l | grep -q '/var/lib/hemAppBin/supportWare/cleanHemLog.sh'; then
	echo "Adding cronjob"
	(crontab -u hem -l; echo "$line" ) | crontab -u hem -
else 
	echo "Cronjob already exits"
fi