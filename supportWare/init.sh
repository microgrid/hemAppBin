#!/bin/bash
sleep 90
# Enable I2C
#echo BB-I2C1 > /sys/devices/platform/bone_capemgr/slots
#Setup RTC
#echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-2/new_device
#Setup system time from hardware clock
hwclock -s -f /dev/rtc1
#Mount SD card
sleep 5
mount.exfat /dev/mmcblk1p1 /media/card
sleep 1
mount.exfat /dev/mmcblk0p1 /media/card
#Start monit daemon
monit -c /var/lib/hemAppBin/supportWare/monitrc
#export PATH
#sudo su
cd /var/lib/hemAppBin
mkdir -p log
ulimit -c unlimited
export LD_LIBRARY_PATH=.
./hemapp
