#!/usr/bin/env bash
sudo modprobe fbtft_device busnum=1 name=adafruit28 debug=7 verbose=3 rotate=90 gpios=dc:48,reset:60
sudo insmod /lib/modules/$(uname -r)/extra/ft6236.ko
